import os
import time
import configparser
from datetime import datetime

import pymssql
import pandas as pd
import geopandas as gpd
import shapely.wkt
# import openpyxl # needed for env for export in pandas


class Database:
    """Class handles all database related function"""
    def __init__(self, database_name, usr):
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/{usr.lower()}/{usr.lower()}.ini'
        self.ini_database_name = database_name.upper()

    def parse_db_credentials(self):
        """
        fetches db credentials from ini file
        :return: db credentials
        """
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config[f'{self.ini_database_name}']['userid']
        pw_read = config[f'{self.ini_database_name}']['password']
        host = config[f'{self.ini_database_name}']['host']
        port = config[f'{self.ini_database_name}']['port']
        dbname = config[f'{self.ini_database_name}']['databasename']

        credentials = dict()
        credentials['usr'] = usr_read
        credentials['pw'] = pw_read
        credentials['host'] = host
        credentials['port'] = port
        credentials['dbname'] = dbname

        return credentials

    def connect_to_mssql_db(self):
        cred = self.parse_db_credentials()
        database = cred['dbname']

        try:
            mssql_conn = pymssql.connect(cred['host'], cred['usr'], cred['pw'], cred['dbname'])
            del cred
        except Exception as e:
            print(e)
            raise ValueError(f'Unable to connect to mssql database: {database}')
        else:
            return mssql_conn, database

    def import_mssql_to_df(self, sql):
        """
        Runs a query a mssql server database and store the output in a pandas dataframe
        :param sql: sql query for the import
        :return: pandas dataframe containing output from sql query
        """

        conn, database = self.connect_to_mssql_db()
        try:
            df_mssql = pd.read_sql(sql, conn)
            conn.close()
        except Exception as e:
            print(f'\n{e}')
            raise ValueError(f'Unable to fetch dataframe from database: {database}')
        else:
            return df_mssql


class Shapefile:

    def __init__(self, shapefile_path):
        self.shapefile_path = shapefile_path

    def shapefile_to_geodataframe(self):
        gdf = gpd.read_file(self.shapefile_path)

        return gdf

    def geodataframe_to_wellknowntext(self):
        shp_gdf = self.shapefile_to_geodataframe()
        shp_dissolve = shp_gdf.dissolve()
        shp_wkt = shp_dissolve.to_wkt()

        return shp_wkt

    def projection_from_gdf(self):
        shp_gdf = self.shapefile_to_geodataframe()
        geom_srid_num = shp_gdf.crs.to_epsg()

        return geom_srid_num


class Ois:
    def __init__(self, shp_path):
        self.shp_path = shp_path
        self.shp_wkt_str, self.shp_srid = self.fetch_wkt_and_srid_from_shapefile()
        self.filename = os.path.splitext(os.path.basename(self.shp_path))[0]

    @staticmethod
    def get_df_name(df):
        name = [x for x in globals() if globals()[x] is df][0]
        return name

    def fetch_wkt_and_srid_from_shapefile(self):
        shp = Shapefile(self.shp_path)
        shp_wkt = shp.geodataframe_to_wellknowntext()
        shp_wkt_str = shp_wkt['geometry'].to_string(index=False)
        shp_srid = shp.projection_from_gdf()

        return shp_wkt_str, shp_srid

    def fetch_sql(self):
        sql_lois = f'''
            WITH 
            geoms AS
                (
                    SELECT geometry::STGeomFromText('{self.shp_wkt_str}', {self.shp_srid}) AS geom
                )
            SELECT 
                Matrikelnummer,
                BFEnummer,
                Ejerlavskode,
                Kommunenavn,
                PrimaerKontakt_Navn AS EJER_NAVN,
                PrimaerKontakt_coNavn AS EJER_CONAVN,
                concat(PrimaerKontakt_Vejnavn, ' ', PrimaerKontakt_HusNr, ',' + PrimaerKontakt_Etagebetegnelse + '.', ',-' + PrimaerKontakt_Doerbetegnelse) AS EJER_ADR,
                PrimaerKontakt_SuppByNavn AS EJER_UDV_ADR,
                concat(PrimaerKontakt_Postnr, ' ' + PrimaerKontakt_PostDistrikt) AS EJER_POSTADR,
                PrimaerKontakt_Beskyttelse_T AS EJER_ADR_BESKYT_T,
                PrimaerKontakt_EjerForholdsK_T AS EJERFORHOLD_KODE_T,
                AntalEjer,
                landbrugsnotering,
                j.Geometri.STAsText() AS geom_wkt
            FROM OIS_CFK.MATRIKEL.JordstykkeEjerGeoView j
            INNER JOIN geoms ON j.Geometri.STIntersects(geoms.geom) = 1
            ORDER BY PrimaerKontakt_Navn
        '''

        return sql_lois

    def fetch_ois_owner_information(self):
        sql_lois = self.fetch_sql()
        print(sql_lois)
        db_msois = Database(database_name='MSSQLOIS', usr='reader')
        df_owners = db_msois.import_mssql_to_df(sql=sql_lois)

        # rearrange the name of the owner
        # df_owners[['LAST_NAME', 'FIRST_NAME']] = df_owners['EJER_NAVN'].str.split(',', expand=True)  # Split the names
        # df_owners['LAST_NAME'] = df_owners['LAST_NAME'].str.strip()  # Strip whitespace from last name
        # df_owners['FIRST_NAME'] = df_owners['FIRST_NAME'].str.strip()  # Strip whitespace from first name
        # # df_owners['EJER_NAVN'] = df_owners['FIRST_NAME'] + ' ' + df_owners['LAST_NAME']  # Rearrange the names
        # # df_owners['REARRANGED_NAME'] = df_owners['FIRST_NAME'] + ' ' + df_owners['LAST_NAME']
        # df_owners['EJER_NAVN'] = (df_owners['FIRST_NAME'].fillna('') + ' ' + df_owners['LAST_NAME']).str.strip()
        # df_owners.loc[df_owners['EJER_NAVN'].str.contains(',', na=False), 'REARRANGED_NAME'] = df_owners['FIRST_NAME'] + ' ' + df_owners['LAST_NAME']
        # df_owners.pop('LAST_NAME')  # delete column LAST_NAME
        # df_owners.pop('FIRST_NAME')  # delete column FIRST_NAME

        # fetch unique name, co-name, zipcode and adress from the owner dataframe
        df_owners_unique = df_owners.loc[:, ['EJER_NAVN', 'EJER_CONAVN', 'EJER_POSTADR', 'EJER_ADR']].drop_duplicates()

        geometry = df_owners['geom_wkt'].map(shapely.wkt.loads)
        df_2 = df_owners.drop('geom_wkt', axis=1)
        gdf_owners = gpd.GeoDataFrame(df_2, crs=f"EPSG:{self.shp_srid}", geometry=geometry)

        return df_owners, df_owners_unique, gdf_owners

    def export_ois_owner_information_to_xlsx(self):
        df_ois, df_ois_unique_owners, gdf_owners = self.fetch_ois_owner_information()

        timestamp = datetime.now().strftime("%Y%m%d%H%M%S")  # current date and time

        # export ois information to xlsx
        path_output_ois = f'./{self.filename}_ois_lodsejeroplysninger_{timestamp}'
        df_ois.to_excel(excel_writer=f'{path_output_ois}.xlsx', sheet_name='OIS Ejerinformation', index=False)

        # export ois information to shapefile (NB truncated column-names if above 10 chars)
        gdf_owners.to_file(filename=f'{path_output_ois}.shp')

        # export unique owners from ois information to xlsx
        path_output_ois_unique_owners = f'./{self.filename}_ois_lodsejeroplysninger_grupperet_til_eboks__{timestamp}.xlsx'
        df_ois_unique_owners.to_excel(
            excel_writer=path_output_ois_unique_owners,
            sheet_name='OIS Ejerinformation grupperet',
            index=False
        )


# start timer
time_start = time.time()

# this is needed for long wkt strings since pandas crop long strings
pd.set_option('display.max_colwidth', 1000000)

# EDIT SHAPEFILE PATH HERE
shp_path = r'F:\GKO\drift\Høje-Taastrup Kommune\synkronpejlerunde_tunhøjsamarbejde\vores GIS\udvalgteboringer_udkastA.shp'

# fetch ois owner information and output to excel
ois_extracter = Ois(shp_path=shp_path)
ois_extracter.export_ois_owner_information_to_xlsx()

# end timer and print time spend for execution
time_elapsed = round(time.time() - time_start, 2)
print(f'\n{os.path.basename(__file__)} executed in {time_elapsed} seconds')
