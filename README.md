# ois_extract

## Description
The program ois_extract is joining cadastral information
with ownership information within the geometry of the input shapefile.

To add CPR to the ois_extract output, O&D has to be contacted via 
[O&D contact](mailto:ertai@mst.dk). CVR data can only be added by single lookup. The output must an attached to mail 
as the excel output format. Addig cpr to OIS data must be approved by your 
immediate superior prior to contacting O&D, 
i.e. [Per Schriver](mailto:persc@mst.dk) 

## Output
- <b> matrikelnummer: </b> The id of the danish land/cadastral, which is unique within the muncipality.
- <b> geom: </b> The geometry of the land/cadastral.
- <b> kommune_nr: </b> the id of the muncipality in which the land/cadastral is situated.
- <b> ejer_status_kode / ejer_status_kode_t: </b> Is the id and description of the owner type e.g. mainowner or co-owner of the land/cadastral.
- <b> ejer_navn: </b> Owner of the land/cadastral.
- <b> ejer_conavn: </b> Co-owner of the land/cadastral.
- <b> ejer_adr: </b> Owner adress, street and street-number, of the land/cadastral.
- <b> ejer_udv_adr: </b> Additional information regarding owner adress of the land/cadastral.
- <b> ejer_postadr: </b> The zip-code and city of the land/cadastral.
- <b> ejerforhold_kode / ejerforhold_kode_t: </b> The id and description of the owner type e.g. private, coorporate og public property
- <b> mat_vejkode : </b> The road code of the cadastrals
- <b> mat_vejnavn : </b> The road name of the cadastrals
- <b> mat_hus_nr : </b> The road number of the cadastrals
- <b> mat_supbynavn : </b> The city subname (area name) of the cadastrals
- <b> mat_postnr : </b> The zip code of the cadastrals
- <b> mat_postbynavn : </b> The city name of the cadastrals corresponding to the zip code  

## Dependencies
The program is depending on the python script within the git repo 
[fetch_ois_data.py](https://gitlab.com/mst-gko/pgois).
The script is setting up local tables daily containing: 
- borehole information (MST-GKO postgres jupiter db)
- cadastral information (Lifa ois db)

## Help
Please see the included documentation, available from the "Help" menu within 
the application, or from the [help](help/help.html) directory.

For Bug and feature requests use gitlab 
[issuetracker](https://gitlab.com/mst-gko/ois_extract/-/issues)

For any other issues contact [Simon Makwarth](mailto:simak@mst.dk)

## Development
The program is written in python using the ois_extract.py script.

## Author and License
Jupiter extract is written by [Simon Makwarth](mailto:simak@mst.dk), and is 
licensed under the GNU Public License version 3 or later. See 
[LICENSE](LICENSE) for details.
