import configparser
import os
import time

import pymssql
import pandas as pd
import shapefile  # conda install pyshp
import pygeoif
import psycopg2 as pg
from pandasql import sqldf

__author__ = 'Simon Makwarth <simak@mst.dk>'
__name__ = 'OIS extract'
__licence__ = 'GNU GPLv3'
__descr__ = f'''
    {__name__} inputs a shapefile, geometry must be point, multipoint, linestring or polygon using srid 25832,
    and outputs a MS excel spreadsheet containing landowner information.
    The landowner information is extracted where the cadastral geometry intersects the shapefile geometry.
'''
__dependencies__ = f'''{__name__} depends git repo pgois which is executed daily on MST-GKO server 
    url: https://gitlab.com/mst-gko/pgois
'''


class Database:
    """Class handles all database related function"""
    def __init__(self, database_name, usr):
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/{usr.lower()}/{usr.lower()}.ini'
        self.ini_database_name = database_name.upper()

    def parse_db_credentials(self):
        """
        fetches db credentials from ini file
        :return: db credentials
        """
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config[f'{self.ini_database_name}']['userid']
        pw_read = config[f'{self.ini_database_name}']['password']
        host = config[f'{self.ini_database_name}']['host']
        port = config[f'{self.ini_database_name}']['port']
        dbname = config[f'{self.ini_database_name}']['databasename']

        credentials = dict()
        credentials['usr'] = usr_read
        credentials['pw'] = pw_read
        credentials['host'] = host
        credentials['port'] = port
        credentials['dbname'] = dbname

        return credentials

    def connect_to_mssql_db(self):
        cred = self.parse_db_credentials()
        database = cred['dbname']

        try:
            print(f'Connecting to database: {database}...')
            mssql_conn = pymssql.connect(cred['host'], cred['usr'], cred['pw'], cred['dbname'])
            del cred
        except Exception as e:
            print(e)
            raise ValueError(f'Unable to connect to mssql database: {database}')
        else:
            print(f'Connected to database: {database}')
            return mssql_conn, database

    def connect_to_pgsql_db(self):
        cred = self.parse_db_credentials()
        pgdatabase = cred['dbname']
        pgcon = None
        try:
            pgcon = pg.connect(
                    host=cred['host'],
                    port=cred['port'],
                    database=cred['dbname'],
                    user=cred['usr'],
                    password=cred['pw']
            )
        except Exception as e:
            print(e)
        else:
            print(f'Connected to database: {pgdatabase}')

        return pgcon, pgdatabase

    def import_mssql_to_df(self, sql):
        """
        Runs a query a mssql server database and store the output in a pandas dataframe
        :param sql: sql query for the import
        :return: pandas dataframe containing output from sql query
        """

        conn, database = self.connect_to_mssql_db()
        try:
            print(f'Fetching table from {database} to dataframe...')
            df_mssql = pd.read_sql(sql, conn)
            conn.close()
        except Exception as e:
            print(f'\n{e}')
            raise ValueError(f'Unable to fetch dataframe from database: {database}')
        else:
            print(f'Table loaded into dataframe from database: {database}')
            return df_mssql

    def import_pgsql_to_df(self, sql):
        """
        Runs a query a mssql server database and store the output in a pandas dataframe
        :param sql: sql query for the import
        :return: pandas dataframe containing output from sql query
        """

        conn, database = self.connect_to_pgsql_db()
        try:
            print(f'Fetching table from {database} to dataframe...')
            df_pgsql = pd.read_sql(sql, conn)
            conn.close()
        except Exception as e:
            print(f'\n{e}')
            raise ValueError(f'Unable to fetch dataframe from database: {database}')
        else:
            print(f'Table loaded into dataframe from database: {database}')

        return df_pgsql


class Ois:
    def __init__(self, shp_path_input):
        self.shp_path = shp_path_input

    def geom_from_shp_mssql(self):
        sf = shapefile.Reader(self.shp_path)
        cte_shp_geom = ''

        for k, s in enumerate(sf.shapes()):
            if sf.shapeType in [1, 11, 21, 8, 18, 28, 5, 15, 25, 3, 13, 23]:
                poly = pygeoif.geometry.as_shape(s)
            else:
                raise ValueError('Shapefile geometry is not Point, Multipoint, Linestring or Polygon.')

            if k == 0:
                cte_shp_geom += f"""
                                SELECT 
                                    geometry::STGeomFromText('{poly}', 25832) as geom"""
            else:
                cte_shp_geom += f""" 
                                UNION ALL 
                                SELECT 
                                    geometry::STGeomFromText('{poly}', 25832) as geom"""

        return cte_shp_geom

    def geom_from_shp(self):
        sf = shapefile.Reader(self.shp_path)

        cte_shp_geom = ''
        for k, s in enumerate(sf.shapes()):
            poly = pygeoif.geometry.as_shape(s)
            if k == 0:
                cte_shp_geom += f"""
                                    SELECT 
                                    ST_MakeValid(ST_GeomFromText('{poly}', 25832)) as geom"""
            elif k > 0:
                cte_shp_geom += f""" 
                                    UNION ALL 
                                    SELECT
                                        ST_MakeValid(ST_GeomFromText('{poly}', 25832)) as geom"""
            else:
                pass

        return cte_shp_geom

    def fetch_landowner_information(self):

        db_pgois = Database('GRUKOS', 'reader')
        db_msois = Database('MSSQLOIS', 'reader')

        # fetch jordstykke information from MST-GKO postgres ois database
        shp_geom = self.geom_from_shp()
        sql_jordstykke = f'''
                    WITH tmp as ({shp_geom})
                    SELECT 
                        j.esr_Ejendomsnummer,
                        j.matrikelnummer,
                        j.sfe_Registrering,
                        j.l_NoteringsType,
                        j.mat_vejkode,
                        j.mat_vejnavn,
                        j.mat_hus_nr,
                        j.mat_supbynavn,
                        j.mat_postnr,
                        j.mat_postbynavn,
                        ST_AsText(j.geom) AS geom_wkt
                    FROM mst_ois.jordstykke_mat_adr j  
                    INNER JOIN tmp ON ST_Intersects(tmp.geom, j.geom)
                    WHERE j.esr_Ejendomsnummer IS NOT NULL
                '''
        print(sql_jordstykke)
        df_jordstykke = db_pgois.import_pgsql_to_df(sql_jordstykke)
        print(df_jordstykke['esr_ejendomsnummer'])
        # fetch owner information from mssql lifa ois database use esr_Ejendomsnummer from postgres ois
        komedjnr = ''
        for i, val in enumerate(df_jordstykke['esr_ejendomsnummer'].values):
            print(val)
            if val:
                if i != 0:
                    komedjnr += ', '
                komedjnr += '\''
                komedjnr += str(int(val))  # to int because pandas adds .0 to all floats
                komedjnr += '\''

        sql_owner = f'''
            SELECT
                CAST(ev.KomEjdNr AS bigint) AS esr_ejendomsnummer,
                ev.KOMMUNE_NR ,
                ev.Kommune,
                ev.EJER_NAVN,
                ev.EJER_CONAVN,
                ev.EJER_ADR,
                ev.EJER_UDV_ADR,
                ev.EJER_POSTADR,
                ev.EJER_STATUS_KODE_T,
                ev.EJER_ADR_BESKYT_T,
                ev.EJERFORHOLD_KODE_T
            FROM dbo.ESREjerView ev
            WHERE ev.KomEjdNr IN ({komedjnr})
        '''
        print(sql_owner)
        df_owner = db_msois.import_mssql_to_df(sql_owner)
        print(df_owner.head())

        # join jordstykke with owner information using pandasql
        sql_join = f'''
            SELECT 
                j.esr_Ejendomsnummer,
                j.matrikelnummer,
                j.sfe_Registrering,
                j.l_NoteringsType,
                j.mat_vejkode,
                j.mat_vejnavn,
                j.mat_hus_nr,
                j.mat_supbynavn,
                j.mat_postnr,
                j.mat_postbynavn,
                ev.Kommune,
                ev.EJER_NAVN,
                ev.EJER_CONAVN,
                ev.EJER_POSTADR,
                ev.EJER_ADR,
                ev.EJER_UDV_ADR,
                ev.EJER_STATUS_KODE_T,
                ev.EJER_ADR_BESKYT_T,
                ev.EJERFORHOLD_KODE_T,
                j.geom_wkt
            FROM df_jordstykke j
            LEFT JOIN df_owner ev ON j.esr_Ejendomsnummer = ev.esr_Ejendomsnummer
        '''
        df = sqldf(sql_join)
        print(df.head())
        filename = os.path.splitext(os.path.basename(self.shp_path))[0]
        path_excel = f'./{filename}_ois_lodsejeroplysninger.xlsx'
        df.to_excel(excel_writer=path_excel, sheet_name='OIS Ejerinformation', index=False)

        # fetch all unique owners for future cpr query
        sql_grp = f'''
            SELECT DISTINCT
                ev.EJER_NAVN,
                ev.EJER_CONAVN,
                ev.EJER_POSTADR,
                ev.EJER_ADR
            FROM df ev  
        '''
        df_grp = sqldf(sql_grp)
        print(df_grp.head())
        path_excel_grp = f'./{filename}_ois_lodsejeroplysninger_grupperet_til_eboks.xlsx'
        df_grp.to_excel(excel_writer=path_excel_grp, sheet_name='OIS Ejerinformation grupperet', index=False)

        del df, df_grp, df_jordstykke, df_owner, db_pgois, db_msois


# start timer
t = time.time()

# # EDIT SHAPE PATH HERE # #
shp_path = r'F:\Nordjylland\Medarbejdere\SIMAK\temp\ois_udtræk\til_elise_2022_1118\Shp hra Elise\Vandstandslogger_Djurs_Himmerland\Logger_Himmerland_Øst.shp'

ois = Ois(shp_path)
ois.fetch_landowner_information()

# end timer and print time spend for execution
elapsed = round(time.time() - t, 2)
print(f'\n{os.path.basename(__file__)} executed in {elapsed} s')
